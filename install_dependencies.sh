#!/bin/bash




##################### General Notes ###################################33
# - sudo didn't work inside the docker, so if u need to run this command insde docker u should remove all sudo
# # install php
VERSION=7.0
sudo apt-get update -y 
sudo apt-get upgrade -y 
sudo apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update -y 
sudo apt-get install -y php${VERSION}
apt-get install php${VERSION}-fpm php${VERSION}-common php${VERSION}-mysql php${VERSION}-xml php${VERSION}-xmlrpc php${VERSION}-curl php${VERSION}-gd php${VERSION}-imagick php${VERSION}-cli php${VERSION}-dev php${VERSION}-imap php${VERSION}-mbstring php${VERSION}-opcache php${VERSION}-soap php${VERSION}-zip php${VERSION}-intl php${VERSION}-memcached -y

# remove el bedan
sudo service apache2 disable  
sudo service apache2  stop


# install nginx 
 sudo apt install -y nginx 
# enable and start services
# enable and restart services
sudo service php${VERSION}-fpm enable
sudo service nginx  enable
sudo service php${VERSION}-fpm start
sudo service nginx  start
# Vhost Configurations
mv  /etc/nginx/sites-enabled/default ~/default.backup
#rm /etc/nginx/sites-available/default 

cat <<EOF > /etc/nginx/sites-enabled/default.conf
server {
#listen 443 ssl;
#ssl_certificate    /etc/letsencrypt/live/mapjordan.net/fullchain.pem;
#   ssl_certificate_key /etc/letsencrypt/live/mapjordan.net/privkey.pem;
#   ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
        root /var/www/html;
        client_max_body_size 8091m;
        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm  index.nginx-debian.html;
        access_log /var/log/nginx/access.log;
        server_name _;
        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                #try_files \$uri \$uri/ =404;
                try_files \$uri  /index.php?\$query_string;
                client_max_body_size 8091m;
        }
        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php${VERSION}-fpm.sock;
                fastcgi_read_timeout 10000;
        }
        location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
           expires 1M;
           access_log off;
           add_header Cache-Control "public";
        }
   location ~* \.(?:css|js)$ {
        expires 1y;
        access_log off;
        add_header Cache-Control "public";
      }
}
EOF

# changing parameters
 #php
 sudo sed -ri 's/^max_execution_time\s*=\s*30/max_execution_time = 5000M/g' /etc/php/${VERSION}/fpm/php.ini \
 && sudo  sed -ri 's/^max_input_time\s*=\s*60/max_input_time = 5000M/g' /etc/php/${VERSION}/fpm/php.ini \
 && sudo sed -ri 's/^memory_limit\s*=\s*128M/memory_limit = 5000M/g' /etc/php/${VERSION}/fpm/php.ini \
 && sudo sed -ri 's/^post_max_size\s*=\s*8M/post_max_size = 5000M/g' /etc/php/${VERSION}/fpm/php.ini \
 && sudo sed -ri 's/^upload_max_filesize\s*=\s*2M/upload_max_filesize = 5000M/g' /etc/php/${VERSION}/fpm/php.ini \
 && sudo sed -ri 's/^max_file_uploads\s*=\s*20/max_file_uploads = 5000M/g' /etc/php/${VERSION}/fpm/php.ini
# # nginx
 sudo sed -ri 's/^client_max_body_size\s*=\s*2M/client_max_body_size = 5000M/g' /etc/nginx/sites-enabled/default.conf
# restart services
sudo service php${VERSION}-fpm restart
sudo service nginx  restart

# mount EFS

sudo apt install nfs-common -y
sudo mkdir -p /var/www/html/images
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-d3a6f719.efs.eu-west-1.amazonaws.com:/images /var/www/html/images



# install codedeploy agent
REGION=$(curl 169.254.169.254/latest/meta-data/placement/availability-zone/ | sed 's/[a-z]$//')
sudo apt-get update -y
sudo apt-get install wget ruby -y
# depends on your region wget this script
cd /home/ubuntu
wget https://aws-codedeploy-${REGION}.s3.amazonaws.com/latest/install
chmod +x ./install
sudo ./install auto

